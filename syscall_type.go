package stracefd

import syscallnames "gitlab.com/rigel314/stracefd/syscall-names"

//go:generate bash gen/genFdList.sh

func init() {
	// manually add the fd syscall entries that the script didn't find

	// no fd returns are noticed by the script, so add them all here
	fdArgs["accept"] = append(fdArgs["accept"], -1)
	fdArgs["accept4"] = append(fdArgs["accept4"], -1)
	fdArgs["eventfd"] = append(fdArgs["eventfd"], -1)
	fdArgs["eventfd2"] = append(fdArgs["eventfd2"], -1)
	fdArgs["inotify_init"] = append(fdArgs["inotify_init"], -1)
	fdArgs["io_uring_setup"] = append(fdArgs["io_uring_setup"], -1)
	fdArgs["landlock_create_ruleset"] = append(fdArgs["landlock_create_ruleset"], -1)
	fdArgs["memfd_create"] = append(fdArgs["memfd_create"], -1)
	fdArgs["memfd_secret"] = append(fdArgs["memfd_secret"], -1)
	fdArgs["mq_open"] = append(fdArgs["mq_open"], -1)
	fdArgs["msgget"] = append(fdArgs["msgget"], -1)
	fdArgs["open_tree"] = append(fdArgs["open_tree"], -1)
	fdArgs["open"] = append(fdArgs["open"], -1)
	fdArgs["openat"] = append(fdArgs["openat"], -1)
	fdArgs["openat2"] = append(fdArgs["openat2"], -1)
	fdArgs["perf_event_open"] = append(fdArgs["perf_event_open"], -1)
	fdArgs["pidfd_getfd"] = append(fdArgs["pidfd_getfd"], -1)
	fdArgs["pidfd_open"] = append(fdArgs["pidfd_open"], -1)
	fdArgs["shmget"] = append(fdArgs["shmget"], -1)
	fdArgs["signalfd"] = append(fdArgs["signalfd"], -1)
	fdArgs["signalfd4"] = append(fdArgs["signalfd4"], -1)
	fdArgs["socket"] = append(fdArgs["socket"], -1)
	fdArgs["spu_create"] = append(fdArgs["spu_create"], -1)
	fdArgs["timerfd_create"] = append(fdArgs["timerfd_create"], -1)
	fdArgs["userfaultfd"] = append(fdArgs["userfaultfd"], -1)

	// for some reason, the socket api functions in the syscalls.h header just don't have argument names, but the other ~300 syscalls have argument names in the header.
	fdArgs["accept"] = append(fdArgs["accept"], 0)
	fdArgs["accept4"] = append(fdArgs["accept4"], 0)
	fdArgs["bind"] = append(fdArgs["bind"], 0)
	fdArgs["connect"] = append(fdArgs["connect"], 0)
	fdArgs["getpeername"] = append(fdArgs["getpeername"], 0)
	fdArgs["getsockname"] = append(fdArgs["getsockname"], 0)
	fdArgs["listen"] = append(fdArgs["listen"], 0)
	fdArgs["ppoll_time32"] = append(fdArgs["ppoll_time32"], 0)
	fdArgs["ppoll"] = append(fdArgs["ppoll"], 0)
	fdArgs["pselect6_time32"] = append(fdArgs["pselect6_time32"], []int{0, 1, 2, 3}...)
	fdArgs["pselect6"] = append(fdArgs["pselect6"], []int{0, 1, 2, 3}...)
	fdArgs["recv"] = append(fdArgs["recv"], 0)
	fdArgs["recvfrom"] = append(fdArgs["recvfrom"], 0)
	fdArgs["select"] = append(fdArgs["select"], []int{1, 2, 3}...)
	fdArgs["send"] = append(fdArgs["send"], 0)
	fdArgs["sendto"] = append(fdArgs["sendto"], 0)
	fdArgs["shutdown"] = append(fdArgs["shutdown"], 0)
	fdArgs["socketpair"] = append(fdArgs["socketpair"], 3)

	// sendmsg and recvmsg can do weird stuff with ancillary data
	fdArgs["recvmmsg_time32"] = append(fdArgs["recvmmsg_time32"], 1)
	fdArgs["recvmmsg"] = append(fdArgs["recvmmsg"], 1)
	fdArgs["recvmsg"] = append(fdArgs["recvmsg"], 1)
	fdArgs["sendmmsg"] = append(fdArgs["sendmmsg"], 1)
	fdArgs["sendmsg"] = append(fdArgs["sendmsg"], 1)
}

func IsFD(NR syscallnames.NR) (hasfd bool, args []int, retfd bool) {
	if v, ok := fdArgs[NR.String()]; ok {
		hasfd = true
		retfd = false
		for _, a := range v {
			if a >= 0 {
				args = append(args, a)
			} else {
				retfd = true
			}
		}
		return true, nil, retfd
	}

	return false, nil, false
}
