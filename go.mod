module gitlab.com/rigel314/stracefd

go 1.21.0

require (
	golang.org/x/sys v0.12.0
	golang.org/x/tools v0.13.0
)

require golang.org/x/mod v0.12.0 // indirect
