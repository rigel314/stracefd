package syscallnames

//go:generate bash gen/genSyscallNames.sh

func Name(syscallNR NR) string {
	return syscallNR.String()
}
