#!/bin/bash

rm zsysnum_linux* syscallNumber_string*
cp $(go env GOMODCACHE)/golang.org/x/sys@v0.12.0/unix/zsysnum_linux* .
for i in zsysnum_linux*; do
	echo $i
	sed -i -r \
		-e 's/^package.*/package syscallnames\n\ntype NR int/' \
		-e 's/=/NR =/' \
		-e 's#(^.*SYS_([^ ]+).*)#\1 // \L\2#' \
		"$i"
	tmp="${i#zsysnum_linux_}"
	arch="${tmp%.go}"
	go run golang.org/x/tools/cmd/stringer -linecomment -output "syscallNumber_string_linux_$arch.go" -type NR "$i"
done
