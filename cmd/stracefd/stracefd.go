package main

// inspired by https://github.com/lizrice/strace-from-scratch/tree/master

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"runtime/debug"

	"gitlab.com/rigel314/stracefd"
	syscallinternals "gitlab.com/rigel314/stracefd/syscall-internals"
	syscallnames "gitlab.com/rigel314/stracefd/syscall-names"
	"golang.org/x/sys/unix"
)

var (
	pid      = flag.Int("pid", -1, "attach to running process")
	threads  = flag.Bool("f", false, "attach to all threads of pid")
	children = flag.Bool("t", false, "attach to all child threads/processes of pid")
	filename = flag.String("name", "", "trace specific file name")  // TODO: support multiple
	fd       = flag.Int("fd", -1, "trace specific file descriptor") // TODO: support multiple
)

func main() {
	log.SetFlags(log.Lmicroseconds)
	log.Println("stracefd version:", getVersion())
	flag.Parse()

	var regs unix.PtraceRegs

	if *pid == -1 {
		args := flag.Args()
		log.Printf("Run %v\n", args)

		cmd := exec.Command(args[0], args[1:]...)
		cmd.Stderr = os.Stderr
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.SysProcAttr = &unix.SysProcAttr{
			Ptrace: true,
		}
		runtime.LockOSThread()
		defer runtime.UnlockOSThread()
		cmd.Start()
		*pid = cmd.Process.Pid
	} else {
		runtime.LockOSThread()
		defer runtime.UnlockOSThread()
		err := unix.PtraceAttach(*pid)
		if err != nil {
			panic(err)
		}
	}

	_, err := unix.Wait4(*pid, nil, 0, nil)
	if err != nil {
		panic(err)
	}

	syscall_exiting := true
	skip_exit := false

	for {
		if !skip_exit {
			err := unix.PtraceGetRegs(*pid, &regs)
			if err != nil {
				panic(err)
			}

			md := syscallinternals.GetInternals(&regs)

			args := ""
			comma := ""
			if stracefd.ArgCount(md.NR) == -1 {
				args = "invalid_ArgCount"
			}
			for i, v := range md.Args {
				if i >= stracefd.ArgCount(md.NR) {
					break
				}
				args += fmt.Sprintf("%s0x%x", comma, v)
				comma = ", "
			}

			if hasfd, argsfd, retfd := stracefd.IsFD(md.NR); hasfd {
				_, _ = argsfd, retfd
				if !syscall_exiting {
					log.Printf("%s(%s)\n", md.NR, args)
				} else {
					log.Printf("%s(%s) = 0x%x(%d)\n", md.NR, args, md.Ret, int64(md.Ret))
				}
			} else {
				// log.Printf("%s\n", md.NR)
				skip_exit = true
			}

			if md.NR == syscallnames.SYS_EXIT || md.NR == syscallnames.SYS_EXIT_GROUP {
				break
			}
		} else {
			skip_exit = false
		}
		syscall_exiting = !syscall_exiting
		err = unix.PtraceSyscall(*pid, 0)
		if err != nil {
			panic(err)
		}

		_, err = unix.Wait4(*pid, nil, 0, nil)
		if err != nil {
			panic(err)
		}
	}
}

// getVersion returns the git hash of the source used to build the executable or "Uncontrolled"
func getVersion() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		return "Uncontrolled"
	}

	for _, v := range info.Settings {
		if v.Key == "vcs.revision" {
			return v.Value
		}
	}
	return "Uncontrolled"
}
