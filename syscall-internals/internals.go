package syscallinternals

import (
	syscallnames "gitlab.com/rigel314/stracefd/syscall-names"
	"golang.org/x/sys/unix"
)

type Internals struct {
	NR   syscallnames.NR
	Ret  uintptr
	Args []uintptr
}

func GetInternals(regs *unix.PtraceRegs) *Internals {
	ret := &Internals{}
	ret.NR = archGetNR(regs)
	ret.Ret = archGetRet(regs)
	ret.Args = archGetArgs(regs)

	return ret
}
