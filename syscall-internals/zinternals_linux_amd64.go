package syscallinternals

import (
	syscallnames "gitlab.com/rigel314/stracefd/syscall-names"
	"golang.org/x/sys/unix"
)

func archGetNR(regs *unix.PtraceRegs) syscallnames.NR {
	return syscallnames.NR(regs.Orig_rax)
}
func archGetRet(regs *unix.PtraceRegs) uintptr {
	return uintptr(regs.Rax)
}

func archGetArgs(regs *unix.PtraceRegs) []uintptr {
	return []uintptr{
		uintptr(regs.Rdi),
		uintptr(regs.Rsi),
		uintptr(regs.Rdx),
		uintptr(regs.R10),
		uintptr(regs.R8),
		uintptr(regs.R9),
	}
}
