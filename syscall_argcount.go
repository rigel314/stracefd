package stracefd

import syscallnames "gitlab.com/rigel314/stracefd/syscall-names"

//go:generate bash gen/genArgCount.sh

func init() {
	// manually add the arg counts that the script didn't find
	argCounts["arch_prctl"] = 2
}

// ArgCount returns the number of arguments in a given syscall, mainly grabbed from: https://elixir.bootlin.com/linux/v6.5.5/source/include/linux/syscalls.h#L316
func ArgCount(NR syscallnames.NR) int {
	if v, ok := argCounts[NR.String()]; ok {
		return v
	}

	return -1
}
