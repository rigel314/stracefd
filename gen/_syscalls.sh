#!/bin/bash

find linux -type f -exec cat {} + | tr -d '\n' | tr ';' '\n' | grep -P '^asmlinkage.*\bsys_' | sort
