#!/bin/bash

syscalls="$($(dirname $BASH_SOURCE)/_syscalls.sh)"

awkcmd='{
	match($0, /^asmlinkage long sys_([^(]+)\(([^)]+)\)/, parts)
	n = split(parts[2], args, ",");
	fdstr = ""
	for (i = 1; i <= n; i++) {
		if (match(args[i], /(fd\>|fildes)/)) {
			if (fdstr != "") {
				fdstr = (fdstr ",")
			}
			fdstr = (fdstr i)
		}
	}
	if (fdstr != "") {
		print "\"" parts[1] "\": {" fdstr "},";
	}
}'

echo -e "package stracefd\nvar fdArgs = map[string][]int {" > syscall_fdargs_list.go

# echo "$syscalls" | grep -P '(fd\b|fildes)' | awk "$awkcmd"
echo "$syscalls" | awk "$awkcmd" >> syscall_fdargs_list.go

echo -e "}" >> syscall_fdargs_list.go

gofmt -w syscall_fdargs_list.go
