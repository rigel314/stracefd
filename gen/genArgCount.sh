#!/bin/bash

syscalls="$($(dirname $BASH_SOURCE)/_syscalls.sh)"

awkcmd='{
	match($0, /^asmlinkage long sys_([^(]+)\(([^)]+)\)/, parts)
	n = split(parts[2], args, ",");
	print "\"" parts[1] "\": " n ",";
}'

echo -e "package stracefd\nvar argCounts = map[string]int {" > syscall_argcount_list.go

echo "$syscalls" | awk "$awkcmd" >> syscall_argcount_list.go

echo -e "}" >> syscall_argcount_list.go

gofmt -w syscall_argcount_list.go
